package com.testlinking;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class SettingActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        if (getIntent() != null) {
            TextView textView = findViewById(R.id.textView2);
            textView.setText("id = " + getIntent().getStringExtra("id"));
        }
    }
}
